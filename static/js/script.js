var myMap;
var bigMap = false;
var showBy = 10;
var currPage = 0;
var sortDir = 'expensive';

$(function () {

    if($(window).width() >= 768) {
        playMainVideo();
    }
    openPopup();
    closePopup();
    scrollHeader();
    scrollToTop();
    createRangeFilter();
    changeDropdown();
    progressUpdateVideo();
    if($('#filter-map').length) {
        ymaps.ready(initMapFilter);
    }

    $('.c-custom-select').SumoSelect();
    $('.c-custom-scroll').mCustomScrollbar();

    $('body').on('input', '.c-input-filter', function (e) {
        e.preventDefault();
        sendDataFilter();
    });
    $('body').on('change', '.c-custom-select', function (e) {
        e.preventDefault();
        sendDataFilter();
    });
    $('body').on('change', '.c-custom-checkbox', function (e) {
        e.preventDefault();
        sendDataFilter();
    });
    $('body').on('click', '.c-send-form', function (e) {
        e.preventDefault();
        sendDataSubscr();
    });
    $('body').on('click', '.c-pagination-item', function (e) {
        e.preventDefault();
        showBy = $(this).data('content');
        sendDataFilter();
    });
    $('body').on('click', '.c-pagination-link', function (e) {
        e.preventDefault();
        currPage =  $(this).data('content');
        sendDataFilter();
    });
    $('body').on('click', '.c-sort', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        if(!($(this).hasClass('active'))) {
           $(this).attr('data-direction', 'cheaper');
            sortDir = 'cheaper';
        } else {
            $(this).attr('data-direction', 'expensive');
            sortDir = 'expensive';
        }
        sendDataFilter();
    });

});
function showMarkMap() {
    var objectManager = new ymaps.ObjectManager({
        clusterize: true
    });

    jQuery.getJSON('/ajax/data.json', function (json) {
        objectManager.add(json);
        myMap.setBounds(objectManager.getBounds());
    });

    myMap.geoObjects.add(objectManager);
}

function progressUpdateVideo() {
    $('.c-video').bind('timeupdate', function () {
        var currentTime = this.currentTime;
        var positionBar = $(this).parent('.c-video-item').find('.positionBar');
        var duration = this.duration;
        $(positionBar).css({width : (currentTime / duration * 100)  + "%"});
    });
}
function playMainVideo() {
    $('.c-video-item').mouseover(function () {
        var Player = $(this).find('.c-video')[0];
        var interval;
        var hideImg = $(this).find('.c-hide-img');
        hideImg.hide();
        Player.play();
        interval = setInterval(function () {
            if (Player.ended == true) {
                hideImg.show();
                clearInterval(interval);
            }
        }, 2000);
    });
    $('.c-video-item').mouseout(function () {
        var Player = $(this).find('.c-video')[0];
        $(this).find('.c-hide-img').show();
        Player.paused;
    });
}

function openPopup() {
    $('body').on('click', '.c-popup-open', function () {
        $(this).siblings('.c-popup').addClass('open');
        $('body, html').addClass('no-scroll');
    });
}
function closePopup() {
    $('body').on('click', '.c-popup-close', function () {
        $(this).closest('.c-popup').removeClass('open');
        $('body, html').removeClass('no-scroll');
    });
}

function scrollHeader() {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 65) {
            $('.c-top-panel').addClass('fixed');
            $('.c-top-panel-alternate').show();
        } else if ($(window).scrollTop() < 65) {
            $('.c-top-panel').removeClass('fixed');
            $('.c-top-panel-alternate').hide();
        }
    });
}

function scrollToTop() {
    $('.c-arrow-top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 1000);
    });
}
function createRangeFilter() {
    $("#price").slider({
        orientation: "gorizontal",
        range: true,
        values: [1, 30],
        max: 30,
        min: 1,
        slide: function (event, ui) {
            $("#priceFrom").val(ui.values[0]);
            $("#priceTo").val(ui.values[1]);

        },
        stop: function (event, ui) {
            $("#priceFrom").trigger('change');
            $("#priceTo").trigger('change');
            sendDataFilter();

        }
    });
    $("#priceFrom").val($("#price").slider("values", 0));
    $("#priceTo").val($("#price").slider("values", 1));
}

function changeDropdown() {
    var linkHeight = 0;
    var linkFilter = '';
    var textDropdown = '';
    $('body').on('click', '.c-dropdown-link', function (e) {
        e.preventDefault();
        linkFilter = $(this).siblings('.c-dropdown-content');
        $(linkFilter).toggleClass('open');
        $(this).toggleClass('open-link');
    });

    $('body').on('change', '.c-custom-checkbox', function () {
        textDropdown =  $(this).siblings('.c-custom-label').text();
        linkDropdown = $(this).closest('.c-dropdown-content').siblings('.c-dropdown-link').find('.c-input-filter-name');
        $(linkDropdown).html(textDropdown);
    });
}
function initMapFilter(){
    myMap = new ymaps.Map("filter-map", {
        center: [55.704550, 37.571596],
        zoom: 3,
        controls: []
    });
    myMap.behaviors.disable('scrollZoom');

    $('.c-show-on-map').click(toggle);
}

function toggle() {
    bigMap = !bigMap;
    if (bigMap) {
        $('.c-filter-map').removeClass('open-map');
        $('.c-detected-map').val('close');
        myMap.container.fitToViewport();
        sendDataFilter();
    } else {
        $('.c-filter-map').addClass('open-map');
        $('.c-detected-map').val('open');
        myMap.container.fitToViewport();
        sendDataFilter();
        showMarkMap();
    }
}
function sendDataFilter() {
    $('.c-preloader').show();
    var data = $('.c-filter-form').serialize();
    data = data + '&currPage=' + currPage + '&showBy=' + showBy + '&sortDir=' + sortDir;
    console.log(data);
    $.ajax({
        url: '../ajax/result-filter.php',
        type: 'GET',
        dataType: 'html',
        data: data,
        success: function (data) {
            $('.c-filter-result').html(data);
        },
        complete: function () {
            $('.c-preloader').hide();
        },
    })
}
function sendDataSubscr() {
    $('.c-preloader').show();
    var data = $('.c-subscr-form').serialize();
    $.ajax({
        type: 'GET',
        data: data,
        success: function (data) {
            $('.c-subscr-form').html("<div class='send-data'>Данные успешно отправлены</div>");
        },
        complete: function () {
            $('.c-preloader').hide();
        },
    })
}


// скрипты на детальной странице
$(function () {
    $('body').on('click', '.c-dropdownform__link-custom', function(e){
        e.preventDefault();
        $('.c-dropdownform__link-custom').not($(this)).removeClass('active');
        $('.c-dropdownform__content-custom').slideUp(300);
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.c-dropdownform__content-custom').slideUp(300);
        } else {
            $(this).addClass('active');
            $(this).next('.c-dropdownform__content-custom').slideDown(300);
        }
    });
    $('body').on('click', '.c-dropdownform__content-custom .c-aside-close', function(e) {
        e.preventDefault();
        $(this).parent('.c-dropdownform__content-custom').slideUp(300);
        $(this).parent('.c-dropdownform__content-custom').siblings('.c-dropdownform__link-custom').removeClass('active');
    });
});


// скрипты для детальной мобильной
$(function () {
    function sliderIn(slider_el) {
        if (slider_el != undefined) {
            var slider = slider_el.Swipe({
                startSlide: 0,
                continuous: false,
                transitionEnd: function(index, element) {
                    $('.c-fullscreen').attr('data-slide',index);
                }
            }).data('Swipe');
        }
    }
    function sliderInit(slider_el) {
        if (slider_el != undefined) {
            setTimeout(function(){
                var slider = slider_el.Swipe({
                    startSlide: 0,
                    continuous: false,
                    transitionEnd: function(index, element) {
                        $('.c-fullscreen').attr('data-slide',index);
                    }
                }).data('Swipe');
            },1000);
        }
    }

    var slider_el = $('.swipe'),
        slider = [],
        next_buttons = $('.c-slide_next'),
        prev_buttons = $('.c-slide_prev');
    slider_el.each(function (i){
        slider[i] = $(this).Swipe({
            startSlide: 0,
            continuous: false,
            transitionEnd: function(index, element) {
                $('.c-fullscreen').attr('data-slide',index);
            }
        }).data('Swipe');
        
        $(next_buttons[i]).on('click',function(){
            slider[i].next();
        });
        $(prev_buttons[i]).on('click',function(){
            slider[i].prev();
        });
    });
    var dataSlider = $('#date-slider');
    if(dataSlider.length){
        var buttons = $('.c-date-buttons'),
            ajax_url= buttons.data('ajax-url');

        body.on('click', '.c-change-month, .c-change-year', function(e){
            e.preventDefault();
            var elem = $(e.target);

            if(!elem.hasClass('disable')){
                elem.addClass('active');
                elem.siblings().removeClass('active');

                var _year = buttons.find('.c-change-year.active'),
                    _month = buttons.find('.c-change-month.active');

                showPreloader();

                $.ajax({
                    url: ajax_url,
                    data: { year : _year.data('year'), month: _month.data('month') },
                    dataType: "JSON",
                    success: function(data){
                        var template = '',
                            months = buttons.find('.c-change-month');
                        slider.kill();

                        if (data.images !== undefined) {
                            $.each(data.images, function () {
                                var links = $(this);
                                template += '<div><img src="' + links[0].thumb + '" alt=""></div>';
                            });

                            dataSlider.find('.swipe-wrap').html(template);
                            
                        }

                        if (elem.hasClass('c-change-year')) {
                            if (data.dis_months !== undefined) {
                                months.removeClass('disable');
                                for (i = 0; i < data.dis_months.length; i++) {
                                    months.eq(data.dis_months[i]).addClass('disable');
                                }
                            }
                            months.removeClass('active');
                            months.eq(data.active_month).addClass('active');
                        }
                    },
                    error: function(er){
                        console.log(er)
                    },      
                    complete: function(){       
                        hidePreloader();
                        sliderIn(slider_el);                    
                    }
                });
            }
        });
    }
    var rangeSlider = $('.c-slider_range');
    if(rangeSlider.length) {
        $.each(rangeSlider,function(i){
            var elem = $(rangeSlider[i]),
                slider = elem.find(".c-slider_range__input"),
                slider_container = slider.parents('.c-slider_range'),
                elem_from = slider_container.find('.c-slider_range__from'),
                elem_to = slider_container.find('.c-slider_range__to');
            slider.ionRangeSlider({
                type: slider.data('slider_type') || "double",
                min: slider.data('slider_min'),
                max: slider.data('slider_max'),
                from: slider.data('slider_from'),
                to: slider.data('slider_to'),
                step: slider.data('slider_step'),
                hide_from_to: true,
                onStart: function () {
                    elem_from.val(slider.data("slider_from"));
                    elem_to.val(slider.data("slider_to"));
                }
            });

            slider.on("change", function () {
                var $this = $(this),
                    from = $this.data("from"),
                    to = $this.data("to");
                elem_from.val(from);
                elem_to.val(to);
                if ($('#summ').length) {
                    getCurrPercent($('.c-first-summ'), $('.c-summ-describe'));
                }
            });

            slider_container.find('.c-slider_range__value').focusout(function(){
                var item = slider.data("ionRangeSlider");
                item.update({
                    from: elem_from.val(),
                    to: elem_to.val()
                });
                elem_to.val(item.old_to);
                elem_from.val(item.old_from);
            });
        });
    }

    setTimeout(function(){
        var elem = $('.swipe_popup'),
                contain = $('.page-content')
        contain.animate({opacity: 1},300);
        var slider = elem.Swipe({
            startSlide: $('.c-fullscreen').attr('data-slide'),
            continuous: false
        }).data('Swipe');

        $('.c-slide_next_p').on('click',function(){
            slider.next();
        });

        $('.c-slide_prev_p').on('click',function(){
            slider.prev();
        });
    },1000);
});

