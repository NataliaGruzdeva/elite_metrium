<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if (!CModule::IncludeModule("iblock")) {
	die();
}?>

<?

$dbRes = CIBlockElement::GetList(
	array(),
	array(
		'IBLOCK_ID' => $_REQUEST['IBLOCK_ID'],
		'CODE' => $_REQUEST['ELEMENT_CODE'],
		'ACTIVE' => 'Y',
	),
	false,
	false,
	array(
		'ID',
		'NAME',
		'PREVIEW_PICTURE',
		'PREVIEW_TEXT',
		'PROPERTY_ADDRESS',
		'CODE',
		'PROPERTY_PHOTOS'
	)
);

$object = $dbRes->Fetch();

$dbRes = CIBlockElement::GetProperty(
	$_REQUEST['IBLOCK_ID'],
	$object['ID'],
	array(),
	array(
		'CODE'=>'PHOTOS'
	)
);

while($arProperties = $dbRes->GetNext()){
	$arPhotos[] = CFile::GetPath($arProperties['VALUE']);
}
?>

<div class="page-content">
	<div class="project-block">
		<div class="project-block--content">
			<div class="tabs__content c-tabs">
				<div class="tabs__item active c-tabs-item">
					<div class="swipe swipe_popup" style="visibility: visible;">
						<div class="swipe-wrap">
							<?foreach($arPhotos as $photo) :?>
								<div><img src="<?=$photo?>" alt=""></div>
							<?endforeach;?>
						</div>
					</div>
					<div class="buttons-slider">
						<a class="c-slide_prev_p slider_prev">
							<div class="icon-a41"></div>
						</a>
						<a class="c-slide_next_p slider_next">
							<div class="icon-a42"></div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	setTimeout(function(){
		var elem = $('.swipe_popup'),
				contain = $('.page-content')
		contain.animate({opacity: 1},300);
		var slider = elem.Swipe({
			startSlide: $('.c-fullscreen').attr('data-slide'),
			continuous: false
		}).data('Swipe');

		$('.c-slide_next_p').on('click',function(){
			slider.next();
		});

		$('.c-slide_prev_p').on('click',function(){
			slider.prev();
		});
	},1000);
</script>