<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark tile__special-mark--new-offer"><span class="tile__special-text">Новое предложение</span>
            <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
        </div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark tile__special-mark--special-offer"><span class="tile__special-text">Специальное предложение</span>
            <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
        </div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>
<div class="tile__item">
    <div class="tile__item-content">
        <div style="background: url(images/content/item-img1.jpg) no-repeat; background-size: cover" class="tile__item-img"></div>
        <div class="tile__item-info">
            <div class="tile__info-content">
                <div class="tile__info-name">Санаторий Барвиха</div>
                <div class="tile__info-address">Рублево-Успенское, 7 км. от МКАД</div>
                <div class="tile__info-params">
                    <div class="tile__info-param">2500 кв. м</div>
                    <div class="tile__info-param">75 соток</div>
                    <div class="tile__info-param">Под ключ с мебелью</div>
                </div>
                <div class="tile__info-price">
                    <div class="tile__info-price-one">59 000 000 $</div>
                    <div class="tile__info-price-two">376 573 600 руб.</div>
                </div>
                <div class="tile__info-id">ID объекта: 1944</div>
            </div>
        </div>
        <div class="tile__special-mark"><span class="tile__icon icon-heart"></span></div>
    </div>
</div>
<div class="tile__item-clear"></div>